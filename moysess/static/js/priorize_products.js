function getCookie(name) {
  var cookieValue = null;

  if (document.cookie && document.cookie != '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      if (cookie.substring(0, name.length + 1) == (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }

  return cookieValue;
};

$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    var csrftoken = getCookie('csrftoken');
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
  }
});

var showMessage = function(message) {
  var message_div = $('div.alert-info');

  message_div.html(message);
  message_div.show('slow').delay(1000).hide('slow');
};

$(document).on('click', 'button.btn-primary', function() {
  var product_ids = [];

  $('input:checked').each(function(index) {
    product_ids.push($(this).val())
  });

  $.ajax({
    url: 'priorize/',
    type: 'post',
    dataType: 'json',
    data: { product_ids: product_ids },
    success: function(response) { showMessage(response.message); },
    error: function(xhr, msg, err) { console.log(xhr, msg, err); }
  });
});
