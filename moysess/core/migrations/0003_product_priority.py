# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150819_2147'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='priority',
            field=models.BooleanField(default=False),
        ),
    ]
