from django.test import TestCase
from ..models import Product


class ProductSupport(TestCase):

    def create_product_object(self):
        return Product.objects.create(name='product test')

    def create_product_object_with_status(self):
        return Product.objects.create(name='product status true',
            priority=True)

    def find_product_object(self):
        return Product.objects.first()
