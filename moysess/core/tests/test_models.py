from .product_support import ProductSupport


class ProductTest(ProductSupport):

    def test_product_object(self):
        product = self.create_product_object()

        self.assertEqual(product.name, 'product test')
        self.assertEqual(product.get_absolute_url(),
            '/detail/{0}/'.format(product.id))
        self.assertEqual(product.__unicode__(), 'product test')
