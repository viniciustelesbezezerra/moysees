from django.test import TestCase
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from .product_support import ProductSupport


class UserSupport:

    def login_user(self):
        get_user_model().objects \
            .create_user('teste', 'teste@teste.tld', 'senha123')

        self.client.login(username='teste', password='senha123')


class HomePageTest(ProductSupport, UserSupport):

    def get_response(self):
        self.create_product_object()
        self.response = self.client.get(reverse('home'))

    def test_home_page_not_signed_in(self):
        self.get_response()
        self.assertEqual(self.response.status_code, 302)

    def test_home_page_signed_in(self):
        self.login_user()
        self.get_response()

        self.assertEqual(self.response.status_code, 200)
        self.assertIn('product test', self.response.rendered_content)
        self.assertTemplateUsed(self.response, 'core/product_list.html')


class DetailProductTest(ProductSupport, UserSupport):

    def get_response(self):
        product = self.create_product_object()
        self.response = self.client.get(
            reverse('product_detail', args=(product.id,))
        )

    def test_get_detail_url_not_signed_in(self):
        self.get_response()

        self.assertEqual(self.response.status_code, 302)

    def test_get_detail_url_signed_in(self):
        self.login_user()
        self.get_response()

        self.assertEqual(self.response.status_code, 200)
        self.assertIn('product test', self.response.rendered_content)
        self.assertTemplateUsed(self.response, 'core/product_detail.html')


class StatusProductTest(ProductSupport, UserSupport):

    def get_response(self):
        product = self.create_product_object_with_status()
        self.response = self.client.get(reverse('product_status'))

    def test_get_status_url_not_signed_in(self):
        self.get_response()

        self.assertEqual(self.response.status_code, 302)

    def test_get_status_url_signed_in(self):
        self.login_user()
        self.get_response()

        self.assertEqual(self.response.status_code, 200)
        self.assertNotIn('product test', self.response.rendered_content)
        self.assertIn('product status true', self.response.rendered_content)
        self.assertTemplateUsed(self.response, 'core/product_status.html')


class PriorizeProductTest(ProductSupport, UserSupport):

    def get_response(self):
        self.product = self.create_product_object()
        self.response = self.client.post(
            reverse('product_priorize'),
            {'product_ids[]': [self.product.id,]}
        )

    def test_get_priorize_url_not_signed_in(self):
        self.get_response()

        self.assertEqual(self.response.status_code, 302)

    def test_get_priorize_url_signed_in(self):
        self.login_user()
        self.get_response()

        self.assertEqual(self.response.status_code, 200)
        self.assertIn(
            "{0} Product(s) to process".format("1"),
            self.response.content
        )
