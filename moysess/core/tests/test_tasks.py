from .product_support import ProductSupport
from ..tasks import prioritize


class PrioritizeTest(ProductSupport):

    def test_changed_priority(self):
        product = self.create_product_object()
        self.assertEqual(product.priority, False)

        prioritize([product.id,])

        product = self.find_product_object()
        self.assertEqual(product.priority, True)
