from django.test import TestCase
from ...scripts.seed_products import import_products
from ..models import Product


class ProductImportTest(TestCase):

    def test_product_object(self):
        import_products()

        self.assertEqual(Product.objects.count(), 100)
