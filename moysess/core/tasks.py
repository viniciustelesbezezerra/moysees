from djcelery import celery
from .models import Product


@celery.task(name='default.prioritize')
def prioritize(product_ids):
    Product.objects.filter(id__in=product_ids).update(priority=True)

