from django.db import models
from django.core.urlresolvers import reverse


class Product(models.Model):
    name = models.CharField(max_length=500, unique=True)
    priority = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product_detail', kwargs={'pk': self.pk})
