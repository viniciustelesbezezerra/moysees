from django.views import generic
from braces import views as braces
from django.http import JsonResponse, HttpResponse
from .models import Product
from .tasks import prioritize


class ViewsMixin(braces.LoginRequiredMixin):
    model = Product


class ProductDetail(ViewsMixin, generic.DetailView):
    pass


class IndexView(ViewsMixin, generic.ListView):
    paginate_by = 10


class ProductStatusView(IndexView):
    template_name = 'core/product_status.html'

    def get_queryset(self):
        return Product.objects.filter(priority=True)


class ProductPriorizeView(ViewsMixin, generic.View):

    def post(self, request, *args, **kwargs):
        product_ids = request.POST.getlist("product_ids[]")

        if product_ids:
            message = "{0} Product(s) to process".format(len(product_ids))
            prioritize.delay(product_ids)
        else:
            message = "Nothing to process"

        return HttpResponse(JsonResponse({"message": message}))
