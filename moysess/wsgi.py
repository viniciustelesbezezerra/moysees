import os
from django.core.wsgi import get_wsgi_application
import djcelery

djcelery.setup_loader()

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "moysess.settings")

application = get_wsgi_application()
