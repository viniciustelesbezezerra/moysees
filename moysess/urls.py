from django.conf.urls import include, url
from django.contrib import admin
from .core import views


urlpatterns = [
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^accounts/', include('registration.backends.simple.urls')),

    url(r'^$', views.IndexView.as_view(), name='home'),

    url(r'^status/$', views.ProductStatusView.as_view(),
        name='product_status'),

    url(r'^priorize/$', views.ProductPriorizeView.as_view(),
        name='product_priorize'),

    url(r'^detail/(?P<pk>[0-9]+)/$',
        views.ProductDetail.as_view(), name='product_detail'),
]
